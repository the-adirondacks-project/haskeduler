CREATE TABLE schedule (
  id bigserial PRIMARY KEY,
  hook_id uuid NOT NULL,
  hook_url text NOT NULL,
  run_at_time timestamp with time zone NOT NULL,
  ran_at_time timestamp with time zone NULL,
  created_at timestamp with time zone NOT NULL
);
