CREATE EXTENSION pgcrypto;

ALTER TABLE schedule ALTER COLUMN created_at SET DEFAULT now();
ALTER TABLE schedule ALTER COLUMN hook_id SET DEFAULT gen_random_uuid();
