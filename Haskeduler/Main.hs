{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeOperators #-}

import Data.ByteString.Lazy (ByteString)
import Control.Concurrent (forkIO, threadDelay)
import Control.Exception (Exception, SomeException, throw, try)
import Control.Monad.Error.Class (MonadError)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Reader (MonadIO, MonadReader, ReaderT, asks, forever, runReaderT)
import Data.Aeson (toJSON)
import Data.Maybe (catMaybes)
import Data.Proxy (Proxy(..))
import Data.Semigroup ((<>))
import Data.Text (Text, isPrefixOf, unpack)
import Data.Time (zonedTimeToUTC)
import Data.Time.Clock.POSIX (POSIXTime, getPOSIXTime, utcTimeToPOSIXSeconds)
import Database.PostgreSQL.Simple (Connection, Only(..), connectPostgreSQL, execute, query, query_)
import Network.Wai (Application)
import Network.Wai.Handler.Warp (run)
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Network.Wreq (Response, post)
import Servant (Handler, ServantErr, Server, hoistServer, serve)

import Haskeduler.Server.API
import Haskeduler.Server.API.Types

data DatabaseException = GenericException Text deriving (Show)
instance Exception DatabaseException

scheduleHandler :: ScheduleCreateBody -> RequestHandler (ScheduledHook)
scheduleHandler ScheduleCreateBody{..} = do
  psqlConnection <- asks psqlConnection
  rows <- liftIO $ query psqlConnection
    ("insert into schedule (hook_url, run_at_time) values (?, ?) " <>
    "returning id, created_at, run_at_time, ran_at_time, hook_id, hook_url")
    (hookUrl, runAtTime)
  case rows of
    [] -> throw $ GenericException "Rows is empty"
    [x] -> return x
    _ -> throw $ GenericException "Rows returned more than one"

data RequestHandlerState = RequestHandlerState {
  psqlConnection :: Connection
}

newtype RequestHandler a = RequestHandler {
  runRequestHandlerM :: ReaderT RequestHandlerState Handler a
} deriving (
    Applicative
  , Functor
  , Monad
  , MonadError ServantErr
  , MonadIO
  , MonadReader RequestHandlerState
  )
makeHandler :: RequestHandlerState -> RequestHandler a -> Handler a
makeHandler state x = runReaderT (runRequestHandlerM x) state

api :: Proxy HaskedulerAPI
api = Proxy

server :: RequestHandlerState -> Server HaskedulerAPI
server state = hoistServer api (makeHandler state) scheduleHandler

app :: RequestHandlerState -> Application
app state = serve api (server state)

getState :: IO RequestHandlerState
getState = do
  psqlConnection <- connectPostgreSQL ""
  return $ RequestHandlerState psqlConnection

main :: IO ()
main = do
  state <- getState
  forkIO runHooks
  run 7393 (logStdoutDev (app state))

convertPOSIXTimeToUnix :: POSIXTime -> Int
convertPOSIXTimeToUnix = round . (* 1000)

getUnixTimestamp :: IO (Int)
getUnixTimestamp = convertPOSIXTimeToUnix <$> getPOSIXTime

runHook :: Connection -> ScheduledHook -> IO ()
runHook psqlConnection ScheduledHook{..} = do
  let fixedHookUrl = if "http://" `isPrefixOf` hookUrl || "https://" `isPrefixOf` hookUrl
                       then hookUrl
                       else "http://" <> hookUrl
  response <- try $
    post (unpack fixedHookUrl) (toJSON $ RunHookBody hookId)
    :: IO (Either SomeException (Response ByteString))
  case response of
    Left e -> print e
    Right _ -> return ()
  execute psqlConnection "update schedule set ran_at_time = now() where id = ?" (Only id)
  return ()

runHooks :: IO ()
runHooks = do
  psqlConnection <- connectPostgreSQL ""
  forever $ do
    rows <- query_ psqlConnection
                   ("select id, created_at, run_at_time, ran_at_time, hook_id, hook_url " <>
                   "from schedule where ran_at_time is null") :: IO [ScheduledHook]
    hooksToRun <- mapM (\scheduledHook@ScheduledHook{..} -> do
        let unixTimeToRun = (
                              convertPOSIXTimeToUnix . utcTimeToPOSIXSeconds . zonedTimeToUTC
                            ) runAtTime
        currentTime <- getUnixTimestamp
        if (currentTime + 1000 >= unixTimeToRun)
          then return $ Just scheduledHook
          else return Nothing
      ) rows
    mapM (runHook psqlConnection) (catMaybes hooksToRun)
    threadDelay 100000
