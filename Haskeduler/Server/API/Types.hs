{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Haskeduler.Server.API.Types
(
  RunHookBody(..)
, ScheduleCreateBody(..)
, ScheduledHook(..)
) where

import Data.Aeson (FromJSON(..), ToJSON(..), Value(..), (.=), (.:), object)
import Data.Int (Int64(..))
import Data.Text (Text)
import Data.Time (ZonedTime)
import Data.UUID (UUID)
import Database.PostgreSQL.Simple.FromRow (FromRow(..), field)

data ScheduleCreateBody = ScheduleCreateBody {
  hookUrl :: Text
, runAtTime :: ZonedTime
}

instance FromJSON ScheduleCreateBody where
  parseJSON (Object v) = do
    hookUrl <- v .: "hookUrl"
    runAtTime <- v .: "runAtTime"
    return $ ScheduleCreateBody{hookUrl = hookUrl, runAtTime = runAtTime}

data RunHookBody = RunHookBody {
  id :: UUID
}

instance ToJSON RunHookBody where
  toJSON RunHookBody{..} = object [
      "id" .= id
    ]

data ScheduledHook = ScheduledHook {
  id :: Int64
, createdAt :: ZonedTime
, runAtTime :: ZonedTime
, ranAtTime :: Maybe ZonedTime
, hookId :: UUID
, hookUrl :: Text
} deriving (Show)

instance ToJSON ScheduledHook where
  toJSON ScheduledHook{..} = object [
      "id" .= hookId,
      "runAtTime" .= runAtTime,
      "hookUrl" .= hookUrl
    ]

instance FromRow ScheduledHook where
  fromRow = ScheduledHook <$> field <*> field <*> field <*> field <*> field <*> field

