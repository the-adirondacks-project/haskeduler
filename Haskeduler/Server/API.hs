{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module Haskeduler.Server.API
(
  HaskedulerAPI
) where

import Servant.API (JSON(..), Post(..), ReqBody(..), (:>))

import Haskeduler.Server.API.Types

type HaskedulerAPI = "schedule" :> ReqBody '[JSON] ScheduleCreateBody :> Post '[JSON] ScheduledHook
